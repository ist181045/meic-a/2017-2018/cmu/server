/*
 * MIT License
 *
 * Copyright (c) 2018 Pedro Cerejo, João Oliveira, Rui Ventura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pt.ulisboa.tecnico.meic.cmu.penguins.model.naming;

import com.google.common.base.CaseFormat;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategy;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;

public class SnakeCasePhysicalNamingStrategy implements PhysicalNamingStrategy {

  @Override
  public Identifier toPhysicalCatalogName(Identifier name, JdbcEnvironment jdbcEnvironment) {
    // No need to translate
    return name;
  }

  @Override
  public Identifier toPhysicalSchemaName(Identifier name, JdbcEnvironment jdbcEnvironment) {
    // No need to translate
    return name;
  }

  @Override
  public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment jdbcEnvironment) {
    String snakeCase = formCamelToSnakeCase(name.getText());
    return jdbcEnvironment.getIdentifierHelper().toIdentifier(snakeCase, name.isQuoted());
  }

  @Override
  public Identifier toPhysicalSequenceName(Identifier name, JdbcEnvironment jdbcEnvironment) {
    String snakeCase = formCamelToSnakeCase(name.getText());

    if (!snakeCase.endsWith("_seq")) {
      snakeCase += "_seq";
    }

    return jdbcEnvironment.getIdentifierHelper().toIdentifier(snakeCase, name.isQuoted());
  }

  @Override
  public Identifier toPhysicalColumnName(Identifier name, JdbcEnvironment jdbcEnvironment) {
    String snakeCase = formCamelToSnakeCase(name.getText());
    return jdbcEnvironment.getIdentifierHelper().toIdentifier(snakeCase, name.isQuoted());
  }

  private String formCamelToSnakeCase(String name) {
    return CaseFormat.LOWER_CAMEL
        .to(CaseFormat.LOWER_UNDERSCORE, name)
        .replaceAll("_+", "_");
  }
}
