/*
 * MIT License
 *
 * Copyright (c) 2018 Pedro Cerejo, João Oliveira, Rui Ventura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pt.ulisboa.tecnico.meic.cmu.penguins.handler;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.util.Base64;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import com.google.common.hash.Hashing;
import com.google.common.io.BaseEncoding;
import com.google.common.io.ByteStreams;
import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pt.ulisboa.tecnico.meic.cmu.penguins.model.Location;
import pt.ulisboa.tecnico.meic.cmu.penguins.model.RelayRequest;
import pt.ulisboa.tecnico.meic.cmu.penguins.model.RelayResponse;
import pt.ulisboa.tecnico.meic.cmu.penguins.model.User;
import pt.ulisboa.tecnico.meic.cmu.penguins.server.Server;

public class ResponseHandler implements HttpHandler {

  /*
   * Request type: POST
   * Headers:
   *    Authz: sessionId
   * Parameters: -
   *
   * Expected (JSON):
   *   int locationId: the location's whose quiz is being answered
   *   List questions: location's quiz's questions
   *    [
   *      int id: the question's unique id
   *      int answer: the question's selected answer's unique id
   *    ]
   *    int time: the number of seconds the user took to answer the quiz TODO: not currently
   *    expecting this
   *
   * Returns (JSON):
   *   int score: number of correct questions
   *
   * Error:
   *   Returns error String
   * */
  @Override
  public void handle(HttpExchange exchange) throws IOException {
    if (exchange.getRequestMethod().equals("POST")) {
      boolean relayed = exchange.getRequestHeaders().containsKey("Relayed");

      System.out.printf("ResponseHandler: Received POST from %s%n", exchange.getRemoteAddress());

      String sessionId = exchange.getRequestHeaders().getFirst("Authz");
      if (sessionId == null) {
        String message = String.format("Invalid request from %s%n", exchange.getRemoteAddress());

        System.err.println(message);
        if (relayed) {
          sendRelayResponse(HttpURLConnection.HTTP_BAD_REQUEST, HttpURLConnection.HTTP_BAD_GATEWAY,
              exchange, message);
        } else {
          byte[] bytes = message.getBytes();
          exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, bytes.length);
          exchange.getResponseBody().write(bytes);
          exchange.close();
        }

        return;
      }

      String json;
      RelayRequest request = null;
      if (relayed) {
        KeyStore store;
        try {
          store = KeyStore.getInstance("PKCS12");
          store.load(getClass()
              .getResourceAsStream("/server.jks"), Server.getKeystorePassword());
        } catch (GeneralSecurityException gse) {
          sendRelayResponse(HttpURLConnection.HTTP_INTERNAL_ERROR, exchange, gse.getMessage());
          return;
        }

        try {
          PrivateKey privateKey = ((PrivateKey) store
              .getKey("server", Server.getKeystorePassword()));
          Cipher cipher = Cipher.getInstance(privateKey.getAlgorithm() + "/ECB/PKCS1Padding");
          cipher.init(Cipher.DECRYPT_MODE, privateKey);
          byte[] bytes = ByteStreams.toByteArray(exchange.getRequestBody());
          json = new String(cipher.doFinal(Base64.getDecoder().decode(bytes)));
        } catch (Exception gse) {
          sendRelayResponse(HttpURLConnection.HTTP_BAD_REQUEST, exchange, gse.getMessage());
          return;
        }

        request = new Gson().getAdapter(RelayRequest.class).fromJson(json);
        sessionId = request.getToken();
        String text = request.getToken()
            + request.getKey()
            + request.getIv()
            + request.getContent();
        byte[] hmac = Hashing.hmacSha256(sessionId.getBytes())
            .hashString(text, StandardCharsets.UTF_8)
            .asBytes();
        if (!request.getHmac().equals(BaseEncoding.base64().encode(hmac))) {
          sendRelayResponse(HttpURLConnection.HTTP_BAD_REQUEST, exchange, "Bad request");
          return;
        }

        json = request.getContent();
      } else {
        json = new String(ByteStreams.toByteArray(exchange.getRequestBody()));
      }

      long time;
      int locationId;
      Map<Integer, Integer> questions;
      try {
        JSONObject parameters = new JSONObject(json);
        locationId = parameters.getInt("locationId");
        time = parameters.getLong("time");

        JSONArray questionArray = parameters.getJSONArray("questions");
        questions = IntStream.range(0, questionArray.length())
            .mapToObj(questionArray::getJSONObject)
            .collect(Collectors.toMap(
                q -> q.getInt("id"),
                q -> q.getInt("answer")));
      } catch (JSONException je) {
        exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, -1);
        System.err.printf("Request from %s was invalid%n", exchange.getRemoteAddress());
        return;
      }

      Transaction tx = null;
      String response = "Unknown error occurred";
      int responseCode = HttpURLConnection.HTTP_BAD_REQUEST;

      try (Session session = Server.getSessionFactory().openSession()) {
        tx = session.beginTransaction();

        User user;
        try {
          user = session
              .createQuery("FROM User u WHERE u.session.sessionId = :sessionId",
                  User.class)
              .setParameter("sessionId", sessionId).getSingleResult();
          if (user.getAnsweredQuizzes().contains(locationId)) {
            return;
          }
        } catch (NonUniqueResultException | NoResultException e) {
          response = "Invalid session id";
          responseCode = HttpURLConnection.HTTP_FORBIDDEN;
          return;
        }

        Location location;
        try {
          location = session
              .createQuery("FROM Location WHERE id = :locationId", Location.class)
              .setParameter("locationId", locationId).getSingleResult();
        } catch (NonUniqueResultException | NoResultException e) {
          response = "Invalid location id";
          return;
        }

        int score = (int) location.getQuestions().stream()
            .filter(q -> questions.containsKey(q.getId())) // filter out bad questions
            .mapToLong(q -> q.getAnswers().stream() // filter answers
                .filter(a -> questions.containsValue(a.getId()) && a.isCorrect())
                .count()) // count correct answers
            .sum(); // sum the correct answers (= score)

        user.setScore(user.getScore() + score);
        user.addAnsweredQuizz(locationId);
        user.setTime(user.getTime() + time);
        session.save(user);
        tx.commit();

        response = String.valueOf(score);
        responseCode = HttpURLConnection.HTTP_OK;
      } catch (HibernateException he) {
        if (tx != null) {
          System.err.printf("ResponseHandler: %s has failed an answer submission for location with"
              + " id %s and session id %s", exchange.getRemoteAddress(), locationId, sessionId);
          tx.rollback();
        } else {
          System.err.println("ResponseHandler: transaction creation failed");
        }
      } finally {
        System.out.printf("ResponseHandler: Sending response to %s with code %d and body: %n %s %n",
            exchange.getRemoteAddress(), responseCode, response);
        if (relayed) {
          System.out.println("RELAYED!");
          sendRelayResponse(responseCode, exchange, response, request.getKey(), request.getIv());
        } else {
          exchange.sendResponseHeaders(responseCode, response.getBytes().length);
          exchange.getResponseBody().write(response.getBytes());
          exchange.close();
        }
      }
    } else {
      exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, -1);
      exchange.close();
    }
  }

  private void sendRelayResponse(int code, HttpExchange exchange, String message)
      throws IOException {

    sendRelayResponse(code, exchange, message, null, null);
  }

  private void sendRelayResponse(int relayCode, int srcCode, HttpExchange exchange, String message)
      throws IOException {

    sendRelayResponse(relayCode, srcCode, exchange, message, null, null);
  }

  private void sendRelayResponse(int code, HttpExchange exchange, String message, String key,
      String iv)
      throws IOException {

    sendRelayResponse(HttpURLConnection.HTTP_OK, code, exchange, message, key, iv);
  }

  private void sendRelayResponse(int relayCode, int srcCode, HttpExchange exchange, String message,
      String key, String iv) throws IOException {

    byte[] response = new Gson()
        .getAdapter(RelayResponse.class)
        .toJson(new RelayResponse(srcCode, message)).getBytes();

    if (key != null) {
      SecretKey secretKey = new SecretKeySpec(BaseEncoding.base64().decode(key), "AES");
      IvParameterSpec ivSpec = new IvParameterSpec(BaseEncoding.base64().decode(iv));
      try {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivSpec);
        response = BaseEncoding.base64().encode(cipher.doFinal(response)).getBytes();
      } catch (GeneralSecurityException ignored) {
        response = message.getBytes();
      }
    } else {
      response = message.getBytes();
    }

    String json = new Gson().toJson(new String(response, StandardCharsets.UTF_8.name()));
    exchange.sendResponseHeaders(relayCode, json.getBytes().length);
    exchange.getResponseBody().write(json.getBytes());
    exchange.close();
  }
}
