/*
 * MIT License
 *
 * Copyright (c) 2018 Pedro Cerejo, João Oliveira, Rui Ventura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pt.ulisboa.tecnico.meic.cmu.penguins.handler;

import java.io.IOException;
import java.util.List;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.json.JSONArray;
import org.json.JSONObject;

import pt.ulisboa.tecnico.meic.cmu.penguins.model.User;
import pt.ulisboa.tecnico.meic.cmu.penguins.server.Server;

public class LeaderboardHandler implements HttpHandler {


  /*
   * Request type: GET
   * Headers:
   *    Authz: sessionId
   * Parameters: -
   *
   * Expected (JSON):
   *  -
   *
   * Returns (JSON List):
   *    [
   *      String username: the user's username
   *      int points: the user's score
   *      int time: the user's time
   *      TODO: maybe calculate positions in server and send those, tbd
   *    ]
   *
   * Error:
   *   Returns error String
   * */
  @Override
  public void handle(HttpExchange httpExchange) throws IOException {
    if (httpExchange.getRequestMethod().equals("GET")) {
      System.out.println(String
          .format("LeaderboardHandler: Received GET from %s", httpExchange.getRemoteAddress()));

      String sessionId = httpExchange.getRequestHeaders().get("Authz").get(0);
      if (sessionId == null) {
        httpExchange.sendResponseHeaders(400, -1);
        System.out
            .println(String.format("Request from %s was invalid", httpExchange.getRemoteAddress()));
        return;
      }

      int responseCode = 400;
      String response = "Unknown error occurred";
      Transaction t = null;
      try (Session s = Server.getSessionFactory().openSession()) {
        t = s.beginTransaction();

        String userQueryString = "FROM User u WHERE u.session.sessionId= :sessionId";
        Query<User> userQuery = s.createQuery(userQueryString, User.class)
            .setParameter("sessionId", sessionId);
        List<User> userList = userQuery.list();
        if (userList.size() != 1) {
          response = "Invalid session id";
          responseCode = 400;
          return;
        }

        String queryString = "FROM User";
        Query<User> query = s.createQuery(queryString, User.class);
        List<User> users = query.list();

        users.sort((o1, o2) -> {
          int score1 = o1.getScore();
          int score2 = o2.getScore();

          return score1 == score2
              ? (int) (o1.getTime() - o2.getTime())
              : score2 - score1;
        });

        JSONArray jsonArray = new JSONArray();
        for (User user : users) {
          jsonArray.put(new JSONObject()
              .put("username", user.getUsername())
              .put("points", user.getScore())
              .put("time", user.getTime()));
        }

        responseCode = 200;
        response = jsonArray.toString();

      } catch (HibernateException he) {
        if (t != null) {
          t.rollback();
        } else {
          System.out.println("LeaderboardHandler: transaction creation failed");
        }
      } finally {
        System.out.println(String
            .format("LeaderboardHandler: Sending response to %s with code %d and body: %n %s %n",
                httpExchange.getRemoteAddress(), responseCode, response));
        httpExchange.sendResponseHeaders(responseCode, response.getBytes().length);
        httpExchange.getResponseBody().write(response.getBytes());
        httpExchange.close();
      }
    } else {
      httpExchange.sendResponseHeaders(405, -1);
    }
  }
}
