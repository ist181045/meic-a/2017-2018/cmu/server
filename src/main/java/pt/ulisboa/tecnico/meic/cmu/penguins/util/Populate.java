/*
 * MIT License
 *
 * Copyright (c) 2018 Pedro Cerejo, João Oliveira, Rui Ventura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pt.ulisboa.tecnico.meic.cmu.penguins.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import com.google.common.hash.Hashing;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import pt.ulisboa.tecnico.meic.cmu.penguins.model.Answer;
import pt.ulisboa.tecnico.meic.cmu.penguins.model.Code;
import pt.ulisboa.tecnico.meic.cmu.penguins.model.Coordinates;
import pt.ulisboa.tecnico.meic.cmu.penguins.model.Location;
import pt.ulisboa.tecnico.meic.cmu.penguins.model.Question;
import pt.ulisboa.tecnico.meic.cmu.penguins.server.Server;

public class Populate {


  public static void populateLocations() {
    SessionFactory sessionFactory = Server.getSessionFactory();
    Session s = sessionFactory.openSession();
    Transaction t = null;
    List<Location> locations = new ArrayList<>();

    try {
      String strLine;
      BufferedReader br = new BufferedReader(
          new FileReader(Populate.class.getClassLoader().getResource("locations.txt").getFile()));
      while ((strLine = br.readLine()) != null) {
        String[] locationInfo = strLine.split(",");
        locations.add(new Location(locationInfo[0], locationInfo[1],
            new Coordinates(Float.parseFloat(locationInfo[2]), Float.parseFloat(locationInfo[3]))));
        //System.out.println (strLine);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }

    int n_locations = locations.size();

    try {
      t = s.beginTransaction();

      for (int i = 0; i < n_locations; i++) {
        for (int j = 1; j <= 2; j++) {
          Question question = new Question();
          question.setText(String.format("%s's Question %d", locations.get(i).getName(), j));
          for (int m = 1; m <= 4; m++) {
            Answer answer = new Answer(String.format("Answer %d", m), m == 3);
            question.addAnswer(answer);
          }
          locations.get(i).addQuestion(question);
        }
        s.save(locations.get(i));
      }

      t.commit();
    } catch (HibernateException he) {
      if (t != null) {
        t.rollback();
      }
    } finally {
      s.close();
    }
  }


  public static void populateCodes(int nCodes) {
    SessionFactory sessionFactory = Server.getSessionFactory();
    Transaction t = null;

    try (Session s = sessionFactory.openSession()) {
      t = s.beginTransaction();

      for (int i = 0; i < nCodes; i++) {
        s.save(new Code(Hashing.sha256()
            .hashString("qwerty" + i, StandardCharsets.UTF_8)
            .toString()));
      }

      t.commit();
    } catch (HibernateException he) {
      if (t != null) {
        t.rollback();
      }
    }
  }
}
