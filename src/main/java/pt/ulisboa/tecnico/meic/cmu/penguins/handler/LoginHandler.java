/*
 * MIT License
 *
 * Copyright (c) 2018 Pedro Cerejo, João Oliveira, Rui Ventura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pt.ulisboa.tecnico.meic.cmu.penguins.handler;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.List;

import com.google.common.hash.Hashing;
import com.google.common.io.ByteStreams;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.json.JSONObject;

import pt.ulisboa.tecnico.meic.cmu.penguins.model.User;
import pt.ulisboa.tecnico.meic.cmu.penguins.server.Server;

public class LoginHandler implements HttpHandler {


  /*
   * Request type: POST
   * Headers: -
   * Parameters: -
   *
   * Expected (JSON):
   *   String username: the user's username
   *   String ticketCode: the user's password
   *
   * Returns (JSON):
   *   String sessionId: the session id to be used in other requests
   *
   * Error:
   *   Returns error String
   * */
  @Override
  public void handle(HttpExchange httpExchange) throws IOException {
    if (httpExchange.getRequestMethod().equals("POST")) {
      System.out.println(
          String.format("LoginHandler: Received POST from %s", httpExchange.getRemoteAddress()));
      String json = new String(ByteStreams.toByteArray(httpExchange.getRequestBody()));
      JSONObject parameters = new JSONObject(json);
      String username = parameters.getString("username");
      if (username == null) {
        httpExchange.sendResponseHeaders(400, -1);
        System.out
            .println(String.format("Request from %s was invalid", httpExchange.getRemoteAddress()));
        return;
      }
      String code = Hashing.sha256()
          .hashString(parameters.getString("ticketCode"), StandardCharsets.UTF_8)
          .toString();

      SessionFactory sessionFactory = Server.getSessionFactory();
      Transaction t = null;
      String response = "Unknown error";
      int responseCode = 400;
      try (Session s = sessionFactory.openSession()) {
        t = s.beginTransaction();
        String queryString = "FROM User U WHERE U.username= :username AND U.password= :code";
        Query query = s.createQuery(queryString).setParameter("username", username)
            .setParameter("code", code);
        List results = query.list();
        if (results.size() != 1) {
          responseCode = 401;
          response = "You don't seem to have an account";
          return;
        }
        User user = ((User) results.get(0));
        byte[] bytes = new byte[24];
        new SecureRandom().nextBytes(bytes);
        String sessionId = new String(Base64.getEncoder().encode(bytes));
        user.resetSession(sessionId);
        s.save(user);
        t.commit();

        JSONObject jsonResponse = new JSONObject();
        jsonResponse.put("sessionId", sessionId);

        responseCode = 200;
        response = jsonResponse.toString();
        System.out.println(String.format("LoginHandler: %s has logged in with the username %s",
            httpExchange.getRemoteAddress(), username));
      } catch (HibernateException he) {
        responseCode = 400;
        if (t != null) {
          t.rollback();
          System.out.println(String
              .format("LoginHandler: %s has failed the login with the username %s",
                  httpExchange.getRemoteAddress(), username));
        } else {
          System.out.println("loginHandler: transaction creation has failed");
        }
      } finally {
        httpExchange.sendResponseHeaders(responseCode, response.getBytes().length);
        httpExchange.getResponseBody().write(response.getBytes());
        httpExchange.close();
      }
    } else {
      httpExchange.sendResponseHeaders(405, -1);
    }
  }
}
