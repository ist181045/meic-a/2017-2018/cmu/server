/*
 * MIT License
 *
 * Copyright (c) 2018 Pedro Cerejo, João Oliveira, Rui Ventura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pt.ulisboa.tecnico.meic.cmu.penguins.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.json.JSONObject;

import pt.ulisboa.tecnico.meic.cmu.penguins.model.Answer;
import pt.ulisboa.tecnico.meic.cmu.penguins.model.Location;
import pt.ulisboa.tecnico.meic.cmu.penguins.model.Question;
import pt.ulisboa.tecnico.meic.cmu.penguins.server.Server;
import pt.ulisboa.tecnico.meic.cmu.penguins.util.UrlHelper;

public class QuizHandler implements HttpHandler {


  /*
   * Request type: GET
   * Headers:
   *    Authz: sessionId
   * Parameters: locationId
   *
   * Expected (JSON):
   *   -
   *
   * Returns (JSON List):
   *    [
   *      int id: the question's unique id
   *      String text: the question's textual content
   *      List answers: the question's answers
   *        [
   *          int id: the answer's unique id
   *          String text: the answer's textual content
   *        ]
   *    ]
   *
   * Error:
   *   Returns error String
   * */
  @Override
  public void handle(HttpExchange httpExchange) throws IOException {
    if (httpExchange.getRequestMethod().equals("GET")) {
      System.out.println(
          String.format("QuizHandler: Received GET from %s", httpExchange.getRemoteAddress()));
      byte[] bytes = new byte[666];
      String uri = httpExchange.getRequestURI().getQuery();
      System.out.println(uri);
      String locationId = UrlHelper.UrlQueryParser(uri).get("locationId");
      System.out.println(locationId);
      if (locationId == null) {
        httpExchange.sendResponseHeaders(400, -1);
        System.out
            .println(String.format("Request from %s was invalid", httpExchange.getRemoteAddress()));
        return;
      }

      SessionFactory sessionFactory = Server.getSessionFactory();
      Session s = sessionFactory.openSession();
      Transaction t = null;
      String response = "Unknown error";
      int responseCode = 400;

      try {
        t = s.getTransaction();

        String queryString = "FROM Location WHERE id = :id";
        Query<Location> query = s.createQuery(queryString, Location.class)
            .setParameter("id", Integer.valueOf(locationId));
        List<Location> results = query.list();
        if (results.size() != 1) {
          responseCode = 400; // what's the error code here?
          response = "Invalid location name";
          return;
        }

        Location location = results.get(0);
        List<Map> questions = new ArrayList<>();
        for (Question question :
            location.getQuestions()) {
          Map<String, Object> questionMap = new HashMap<>();
          List<Map<String, Object>> answers = new ArrayList<>();
          for (Answer answer :
              question.getAnswers()) {
            Map<String, Object> answerMap = new HashMap<>();
            answerMap.put("id", answer.getId());
            answerMap.put("text", answer.getText());
            answers.add(answerMap);
          }
          questionMap.put("id", question.getId());
          questionMap.put("text", question.getText());
          questionMap.put("answers", answers);
          questions.add(questionMap);
        }

        JSONObject jsonResponse = new JSONObject();
        jsonResponse.put("name", location.getName());
        jsonResponse.put("questions", questions);

        responseCode = 200;
        response = new Gson().toJson(questions);
      } catch (HibernateException he) {
        if (t != null) {
          t.rollback();
        }
      } finally {
        System.out.println(String
            .format("QuizHandler: Sending response to %s with code %d and body: %n %s %n",
                httpExchange.getRemoteAddress(), responseCode, response));
        httpExchange.sendResponseHeaders(responseCode, response.getBytes().length);
        httpExchange.getResponseBody().write(response.getBytes());
        s.close();
        httpExchange.close();
      }
    } else {
      httpExchange.sendResponseHeaders(405, -1);
    }
  }
}
