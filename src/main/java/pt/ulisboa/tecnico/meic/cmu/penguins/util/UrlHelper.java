/*
 * MIT License
 *
 * Copyright (c) 2018 Pedro Cerejo, João Oliveira, Rui Ventura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pt.ulisboa.tecnico.meic.cmu.penguins.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.common.base.Strings;

public class UrlHelper {

  public static Map<String, String> UrlQueryParser(String query) {
    if (query == null || (query = query.trim()).isEmpty()) {
      return Collections.emptyMap();
    }

    Map<String, String> queryMap = Arrays.stream(query.split("&"))
        .map(String::trim)
        .filter(s -> !Strings.isNullOrEmpty(s))
        .map(s -> s.split("="))
        .filter(pair -> pair.length == 2)
        .collect(Collectors.toMap(
            pair -> pair[0],
            pair -> decode(pair[1], StandardCharsets.UTF_8.name())
        ));

    return Collections.unmodifiableMap(queryMap);
  }

  private static String decode(String s, String enc) {
    try {
      return URLDecoder.decode(s, enc);
    } catch (UnsupportedEncodingException e) {
      return s;
    }
  }
}
