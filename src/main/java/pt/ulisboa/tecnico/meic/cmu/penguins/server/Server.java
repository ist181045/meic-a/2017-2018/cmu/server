/*
 * MIT License
 *
 * Copyright (c) 2018 Pedro Cerejo, João Oliveira, Rui Ventura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pt.ulisboa.tecnico.meic.cmu.penguins.server;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;
import java.util.concurrent.Executors;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.TrustManagerFactory;

import com.google.common.net.HttpHeaders;
import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsParameters;
import com.sun.net.httpserver.HttpsServer;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import pt.ulisboa.tecnico.meic.cmu.penguins.handler.LeaderboardHandler;
import pt.ulisboa.tecnico.meic.cmu.penguins.handler.LocationHandler;
import pt.ulisboa.tecnico.meic.cmu.penguins.handler.LoginHandler;
import pt.ulisboa.tecnico.meic.cmu.penguins.handler.LogoutHandler;
import pt.ulisboa.tecnico.meic.cmu.penguins.handler.QuizHandler;
import pt.ulisboa.tecnico.meic.cmu.penguins.handler.RegisterHandler;
import pt.ulisboa.tecnico.meic.cmu.penguins.handler.ResponseHandler;
import pt.ulisboa.tecnico.meic.cmu.penguins.model.naming.SnakeCasePhysicalNamingStrategy;

public class Server {

  private static final Properties properties = new Properties();

  private static final int HTTP_PORT = 8000;
  private static final int HTTPS_PORT = 44343;
  private static final String KS_PASSWORD = "hoponcmu";

  private static SessionFactory sessionFactory;
  private static char[] ksPassword;

  public static char[] getKeystorePassword() {
    return ksPassword;
  }

  public static void main(String[] args) throws Exception {
    // Bootstrap Hibernate
    Configuration cfg = new Configuration().configure();
    cfg.setPhysicalNamingStrategy(new SnakeCasePhysicalNamingStrategy());
    sessionFactory = cfg.buildSessionFactory();

    // Add hook on shutdown to close sessionFactory
    Runtime.getRuntime().addShutdownHook(new Thread(sessionFactory::close));

    // Load properties if one exists
    try (InputStream input = Server.class.getResourceAsStream("/server.properties")) {
      if (input != null) {
        properties.load(input);
      }
    } catch (IOException ignored) {
    }

    int httpPort =
        Integer.valueOf(properties.getProperty("server.http.port", String.valueOf(HTTP_PORT)));
    int httpsPort =
        Integer.valueOf(properties.getProperty("server.https.port", String.valueOf(HTTPS_PORT)));
    ksPassword = properties.getProperty("server.keystore.password", KS_PASSWORD).toCharArray();

    // Setup HTTP redirect to HTTPS
    HttpServer http = HttpServer.create(new InetSocketAddress(httpPort), 0);
    http.createContext("/", exchange -> {
      String[] host = exchange.getRequestHeaders().getFirst(HttpHeaders.HOST).split(":", 2);
      String url = "https://" + host[0] + ":" + httpsPort;

      exchange.getResponseHeaders().set("Location", url);
      exchange.sendResponseHeaders(HttpURLConnection.HTTP_MOVED_PERM, 0);
      exchange.close();
    });
    http.setExecutor(Executors.newCachedThreadPool());
    http.start();

    // Configure HTTPS server
    SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
    KeyStore store = KeyStore.getInstance("PKCS12");
    store.load(Server.class.getResourceAsStream("/server.jks"), ksPassword);

    KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
    kmf.init(store, ksPassword);

    TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
    tmf.init(store);
    sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

    HttpsServer server = HttpsServer.create(new InetSocketAddress(httpsPort), 0);
    server.setHttpsConfigurator(new HttpsConfigurator(sslContext) {
      @Override
      public void configure(HttpsParameters params) {
        try {
          SSLContext ssl = SSLContext.getDefault();
          SSLEngine engine = ssl.createSSLEngine();
          params.setNeedClientAuth(false);
          params.setCipherSuites(engine.getEnabledCipherSuites());
          params.setProtocols(engine.getEnabledProtocols());

          SSLParameters defaultSSLParameters = ssl.getDefaultSSLParameters();
          params.setSSLParameters(defaultSSLParameters);
        } catch (NoSuchAlgorithmException nsae) {
          throw new RuntimeException(nsae);
        }
      }
    });
    server.setExecutor(Executors.newCachedThreadPool());
    // Set of contexts
    server.createContext("/register", new RegisterHandler());
    server.createContext("/login", new LoginHandler());
    server.createContext("/quiz", new QuizHandler());
    server.createContext("/locations", new LocationHandler());
    server.createContext("/respond", new ResponseHandler());
    server.createContext("/leaderboards", new LeaderboardHandler());
    server.createContext("/logout", new LogoutHandler());
    // Here we go!
    server.start();

    Populate.populateLocations();
    Populate.populateCodes(10);
  }

  public static SessionFactory getSessionFactory() {
    return sessionFactory;
  }
}
